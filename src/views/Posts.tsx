import React, { Component } from 'react';
// import '../style/style.css';
import axios from 'axios';

class PostList extends Component {
    state = {
        posts: []
    }
    componentDidMount() {
        axios.get(`https://jsonplaceholder.typicode.com/posts`)
            .then(res => {
                const posts = res.data;
                this.setState({ posts });
                console.log(posts);
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                <h1>Hello There</h1>
                <ul>{this.state.posts.map(post =>
                    <div>
                        <p>{post.title}</p>
                        <p>{post.body}</p>
                    </div>
                )}</ul>

            </div>
        );
    }
}

export default PostList;